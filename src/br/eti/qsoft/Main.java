package br.eti.qsoft;

import java.io.BufferedReader;
import java.io.FileReader;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONException;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Main {

	private static JSONArray readJson(String fileName) {
		try { 
			FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferReader = new BufferedReader(fileReader);
			StringBuilder stringBuilder = new StringBuilder(); 
			String line;
			while((line = bufferReader.readLine()) != null){
				stringBuilder = stringBuilder.append(line);
			}
			bufferReader.close();
			fileReader.close();
			return new JSONArray(stringBuilder.toString());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static void insertData(MongoDatabase db, String file) throws JSONException {
		System.out.println("Insering data to collection: " + file);
		MongoCollection<Document> collection = db.getCollection(file);
		JSONArray jArray = Main.readJson(file + ".json");
		for(int x=0; x < jArray.length(); x++) {
			collection.insertOne(Document.parse(jArray.getJSONObject(x).toString()));
		}
	}

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		try {
			MongoClientURI uri  = new MongoClientURI("mongodb://easy:carros123@ds229088.mlab.com:29088/easycarros"); 
	        MongoClient client = new MongoClient(uri);
	        MongoDatabase db = client.getDatabase(uri.getDatabase());
			Main.insertData(db, "users");
			Main.insertData(db, "addresses");
			Main.insertData(db, "partners");
			System.out.println("Done");
		} catch (MongoException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

}
